/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      'white': '#ffffff',
      'black': '#000000',
      'dark-primary-color': 'hsl(209.83783783783787, 82.95964125560539%, 52.27450980392157%)',
      'light-secondary-text-color': 'rgba(112, 117, 121, 0.08)',
      'message-highlighting-color': 'hsla(86.4, 43.846153%, 45.117647%, .4)',
      'menu-background-color': 'rgba(255,255,255), var(.85)',
    }
  },
  plugins: [],
}

