export const FileColor =new Map([
    ["docx", "#1B5EBE"],
    ["xlsx", "#1D6F42"],
    ["ppt", "#ED6C47"],
    ["pdf", "#ff0000"]
])