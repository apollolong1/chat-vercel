export const APIURL = `${process.env.REACT_APP_DOMAIN}/${process.env.REACT_APP_API_END_POINT}/${process.env.REACT_APP_API_VERSION}`
export const socketURL = `${process.env.REACT_APP_DOMAIN}/${process.env.REACT_APP_SOCKET_END_POINT}/${process.env.REACT_APP_SOCKET_VERSION}`
export const pathSocket = `/${process.env.REACT_APP_SOCKET_END_POINT}/${process.env.REACT_APP_SOCKET_VERSION}`