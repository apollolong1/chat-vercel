import { Icon } from "../../../../../icon"

export const BackIconBTN = ({onClick}: {onClick?: () => void}) => {
    return <button onClick={onClick} className="w-10 h-10 flex justify-center items-center transition-all hover:bg-light-secondary-text-color text-[#707579] rounded-full">
        <Icon type="arrowLeft" />
    </button>
} 