export const BtnScrollToBottom = ({scrollToBottom}: {scrollToBottom: () => void}) => {
    return <button style={{ boxShadow: '0 1px 8px 1px' }} onClick={scrollToBottom} className="w-[46px] h-[46px] bg-white rounded-full flex items-center justify-center right-4 bottom-4 absolute">
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6">
            <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 13.5 12 21m0 0-7.5-7.5M12 21V3" />
        </svg>
    </button>
}