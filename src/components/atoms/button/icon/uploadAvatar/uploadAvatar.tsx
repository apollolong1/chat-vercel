import { 
    useRef, useState,
} from "react";
import { DialogMolecules } from "../../../../molecules/dialog";
import { Icon } from "../../../../../icon";
import { EditAvatar } from "../../../../molecules";
import { useDispatch } from "react-redux";

export const UploadAvatar = () => {
    const inputRef = useRef<HTMLInputElement>(null);
    const [image, setImage] = useState<any>(null)
    const dispatch = useDispatch()

    const updateAvatar = async (e: string) => {
        if (e) {
            const response = await fetch(e);
            const blob = await response.blob();
            // dispatch(createConversationGroup({
            //     name: nameGroup,
            //     userIds: selectFriend,
            //     thumbnail: blob
            // }))
        }
    }

    return <>
        <input className="hidden" accept="image/*" ref={inputRef} type="file" onChange={e => {
            const file = e.target.files
            if(file && file[0].type.startsWith('image/')) {
                setImage(file[0])
            }
            e.target.value = '';
        }} />
        <button onClick={() => inputRef.current?.click()} className="w-[3.375rem] aspect-square bg-[#3390ec] hover:bg-dark-primary-color transition-all rounded-full flex justify-center items-center right-4 -bottom-6 absolute">
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-6 h-6 text-white">
                <path strokeLinecap="round" strokeLinejoin="round" d="M6.827 6.175A2.31 2.31 0 0 1 5.186 7.23c-.38.054-.757.112-1.134.175C2.999 7.58 2.25 8.507 2.25 9.574V18a2.25 2.25 0 0 0 2.25 2.25h15A2.25 2.25 0 0 0 21.75 18V9.574c0-1.067-.75-1.994-1.802-2.169a47.865 47.865 0 0 0-1.134-.175 2.31 2.31 0 0 1-1.64-1.055l-.822-1.316a2.192 2.192 0 0 0-1.736-1.039 48.774 48.774 0 0 0-5.232 0 2.192 2.192 0 0 0-1.736 1.039l-.821 1.316Z" />
                <path strokeLinecap="round" strokeLinejoin="round" d="M16.5 12.75a4.5 4.5 0 1 1-9 0 4.5 4.5 0 0 1 9 0ZM18.75 10.5h.008v.008h-.008V10.5Z" />
            </svg>
        </button>
        <DialogMolecules open={image !== null} closeModal={() => setImage(null)}>
            <div className="flex flex-col gap-2 items-center py-2">
                <div className="flex gap-4 pl-4 pt-4 items-center w-full">
                    <button onClick={() => setImage(null)} className="transition-all p-2 hover:bg-light-secondary-text-color rounded-full">
                        <Icon type="Xmark" />
                    </button>
                    <span className="font-medium">Chỉnh sửa ảnh đại diện</span>
                </div>
                <EditAvatar image={image} result={e => {
                    setImage(null)
                    // updateAvatar(e)
                }} />
            </div>
            
        </DialogMolecules>
    </>  
}