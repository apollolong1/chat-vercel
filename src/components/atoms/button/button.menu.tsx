import { ReactNode } from "react"

export const ButtonMenu = ({children, onClick}: {children: ReactNode, onClick?: () => void}) => {
    return <button onClick={onClick} className="hover:bg-light-secondary-text-color py-1 px-3 rounded-[.3125rem] text-left transition-all w-full flex-1 mx-1.5 flex gap-4 items-center">
        {children}
    </button>
}