import { useEffect, useRef, useState } from "react";

export const Video = ({url, className}: {url: string, className?: string}) => {
    const videoRef = useRef<HTMLVideoElement>(null);
    const [remainingTime, setRemainingTime] = useState('0:00');
    const [loaded, setLoaded] = useState(false);

    const handleLoad = () => {
      setLoaded(true);
    };

    useEffect(() => {
        if (videoRef.current) {
          videoRef.current.addEventListener('loadedmetadata', () => {
            if(!videoRef.current) return
            const totalSeconds = videoRef.current.duration;
            const minutes = Math.floor(totalSeconds / 60);
            const seconds = Math.round(totalSeconds % 60);
            setRemainingTime(`${minutes}:${seconds < 10 ? '0' : ''}${seconds}`);
          });
    
          videoRef.current.addEventListener('timeupdate', () => {
            if(!videoRef.current) return
            const totalSeconds = videoRef.current.duration - videoRef.current.currentTime;
            const minutes = Math.floor(totalSeconds / 60);
            const seconds = Math.round(totalSeconds % 60);
            setRemainingTime(`${minutes}:${seconds < 10 ? '0' : ''}${seconds}`);
          });
        }
      }, []);

    return <div className={`relative overflow-hidden ${!loaded ? 'animate-pulse' : ''} ${className}`}>
        <span style={{ backgroundColor: 'rgba(0, 0, 0, .35)' }} className="top-[3px] left-[3px] absolute px-[6px] rounded-full flex items-center text-white gap-1">
            {remainingTime}
            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-5 h-5">
                <path strokeLinecap="round" strokeLinejoin="round" d="M17.25 9.75 19.5 12m0 0 2.25 2.25M19.5 12l2.25-2.25M19.5 12l-2.25 2.25m-10.5-6 4.72-4.72a.75.75 0 0 1 1.28.53v15.88a.75.75 0 0 1-1.28.53l-4.72-4.72H4.51c-.88 0-1.704-.507-1.938-1.354A9.009 9.009 0 0 1 2.25 12c0-.83.112-1.633.322-2.396C2.806 8.756 3.63 8.25 4.51 8.25H6.75Z" />
            </svg>
        </span>
        <video ref={videoRef} onLoadedData={handleLoad} autoPlay playsInline loop muted src={url} />
    </div> 
}