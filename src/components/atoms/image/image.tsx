import { useState } from "react";

export const Image = ({url, className}: {url:string, className?: string}) => {
    const [loaded, setLoaded] = useState(false);

    const handleLoad = () => {
        setLoaded(true);
      };

    return <div className={`relative overflow-hidden ${!loaded ? 'animate-pulse' : ''} ${className}`}>
        <img 
            src={url}
            loading="lazy"
            onLoad={handleLoad}
            className="max-h-[340px] h-full max-w-[480px] w-auto"
            alt=""
        />
    </div>
}