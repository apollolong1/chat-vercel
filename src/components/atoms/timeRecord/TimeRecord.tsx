import { useEffect, useState } from "react";
import { useSelector } from "react-redux";

export const TimeRecord = () => {
    const { isRecording } = useSelector(({sendMessage}) => sendMessage)
    const [elapsedTime, setElapsedTime] = useState(0);

    useEffect(() => {
        let interval: NodeJS.Timeout | null = null;
        if (isRecording) {
            interval = setInterval(() => {
                setElapsedTime(oldElapsedTime => oldElapsedTime + 10);
            }, 10);
        } else if (!isRecording && elapsedTime !== 0) {
            if (interval !== null) {
                clearInterval(interval);
            }
        }
        return () => {
            if (interval !== null) {
                clearInterval(interval);
            }
        };
    }, [isRecording, elapsedTime]);
    const formatTime = (time: number) => {
        const date = new Date(time);
        const minutes = date.getMinutes().toString().padStart(2, '0');
        const seconds = date.getSeconds().toString().padStart(2, '0');
        const milliseconds = date.getMilliseconds().toString().padStart(2, '0').substring(0, 2);
        return `${minutes}:${seconds},${milliseconds}`;
    };

    return <div className="h-[2.125rem]">
        <div className="flex gap-2 items-center">
            <span>{formatTime(elapsedTime)} </span>
            <div className="rounded-full w-2.5 h-2.5 bg-[#DF3F40]" />
        </div>
    </div>
}