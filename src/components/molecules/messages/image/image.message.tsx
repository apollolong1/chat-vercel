import { useSelector } from "react-redux"
import { MeMessage } from "../me"
import ReactHtmlParser from 'react-html-parser';
import { convertToHHMM, replaceEmojiAndLinks } from "../../../../services/editMessage";
import { YouMessage } from "../you";
import { Image, Video } from "../../../atoms";
import { Icon } from '@iconify/react';
import { ImageMimeType, VideoMimeType } from "../../../../constants";

export const ImageMessage = ({
    userId,
    createdAt,
    text = '',
    medias = [],
    waiting = false,
}: {
    userId: number,
    createdAt: Date,
    text?: string,
    medias: string[],
    waiting?: boolean,
}) => {
    const { profile } = useSelector(({auth}) => auth)
    const IsMe = profile.id === userId
    
    const image = <div className={`flex flex-col gap-1 max-w-[480px] p-2 rounded-[15px] ${IsMe ? 'rounded-br-none bg-[#e3fee0]' : 'rounded-bl-none bg-white'} relative`}>
        {
            medias.map((element, index) => {
                let parts = element.split('.');
                let extension = parts.pop() ?? '';
                if(ImageMimeType.includes(extension)) {
                    return <Image className="rounded-[15px]" key={index} url={`${process.env.REACT_APP_IMAGE}${element}`} />
                }
                if(VideoMimeType.includes(extension)) {
                    return <Video className="rounded-[15px]" key={index} url={`${process.env.REACT_APP_IMAGE}${element}`} />
                }
                return <div style={{ wordBreak: "break-word" }} key={index} className="whitespace-normal max-w-[480px]">
                    {`${process.env.REACT_APP_IMAGE}${element}`}
                </div> 
            })
        }
        {
            text !== ''
            ? <div className="relative">
                {ReactHtmlParser(replaceEmojiAndLinks(text))}
                <span className="ml-1 translate-y-1.5 text-[12px] float-right text-[#707579] flex items-center gap-0.5">
                    {convertToHHMM(createdAt)}
                    {
                        waiting
                        ? <Icon icon="akar-icons:clock" />
                        : <Icon icon="akar-icons:check" />
                    }
                    {/* <Icon icon="akar-icons:double-check" /> */}
                </span>
            </div>
            : <div style={{ backgroundColor: 'rgba(0, 0, 0, .35)' }} className="text-white bottom-2 right-2 absolute rounded-full px-[.3125rem]">
                {convertToHHMM(createdAt)}
            </div>
        }
    </div>

    if(IsMe) {
        return <MeMessage>
            {image}
        </MeMessage>
    }
    return <YouMessage
        userId={userId}
    >
        {image}
    </YouMessage>
}