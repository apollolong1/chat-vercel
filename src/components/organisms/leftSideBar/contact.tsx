import { ContactHook } from "../../../hooks";
import { BackIconBTN, Button, Search } from "../../atoms";
import { animateScroll as scroll } from 'react-scroll';
import { InfinityScroll } from "../../molecules";
import { useDispatch, useSelector } from "react-redux";
import { createConversationFriend, setFormConversationType, unFriend } from "../../../redux/actions";
import { ItemList } from "../../molecules/ItemList";
import { NavigateFunction, useNavigate } from "react-router-dom";

export const Contact = () => {
    const { search, HandleSearch, loadingInfinity } = ContactHook()
    const { data } = useSelector(({friend}) => friend)
    const dispatch = useDispatch()
    const navigate: NavigateFunction = useNavigate()

    return <div className="w-full h-full max-w-[420px] border-r border-[#dfe1e5] relative z-10">
        <div className="top-0 left-0 w-full h-full z-[1] flex flex-col bg-white absolute">
            <div className="h-[56px] px-4">
                <div className="flex h-full items-center">
                    <div className="h-10 w-10">
                        <BackIconBTN onClick={() => dispatch(setFormConversationType(null))} />
                    </div>
                    <Search value={search} onChange={(e) => {
                        scroll.scrollToTop({ containerId: "left", duration: 0 });
                        HandleSearch(e)
                    } } />
                </div>
            </div>
            <div className="flex-1 relative">
                <div id='left' className="top-0 left-0 w-full h-full absolute overflow-auto scrollbar-show">
                    <InfinityScroll
                        keyRedux="friend" 
                        handle={loadingInfinity} 
                        text='Bạn chưa có ai trong danh bạ'
                        item={
                            value => {
                                console.log(value)
                                const { user: { fullName, lastAvatar }, friendId } = data.get(value)
                                return <div className="w-full relative" >
                                    <div onClick={() => dispatch(createConversationFriend(friendId, navigate))} className="w-full">
                                        <ItemList name={fullName} image={lastAvatar} />
                                    </div>
                                    <div className="right-2 top-1/2 -translate-y-1/2 absolute">
                                        <Button className="px-4 !h-10" onClick={() => dispatch(unFriend(friendId))} variant="text">
                                            Hủy kết bạn
                                        </Button>
                                    </div>
                                </div>
                            } 
                        }
                    />
                </div>
            </div>
        </div>
    </div>
}