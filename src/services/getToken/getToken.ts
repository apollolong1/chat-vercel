import Cookies from 'js-cookie'

export function getAccessToken() {
    return Cookies.get('access_token');
}

export function getRefreshToken() {
    return Cookies.get('refresh_token');
}