export interface IQueryApi {
    page?: number;
    limit?: number;
    keyword?: string;
    type?: string;
    perPage?: number
}
