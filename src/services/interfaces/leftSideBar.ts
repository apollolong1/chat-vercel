export type LeftSideBarType =
    'conversation' | 
    'setting' | 
    'contact' | 
    'friendRequest' | 
    'createGroup' | 
    'searchFriend'|
    null