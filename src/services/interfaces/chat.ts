export type MessageType =
    | "TYPING"
    | "TEXT"
    | "MEDIA"
    | "FILE"
    | "AUDIO"
    | "EMOJI"
    | "LINK"
export type MessageStatus = 
    | "SEEN"
    | "SENT"
    | "SENDING"
