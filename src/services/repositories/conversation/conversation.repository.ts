import { BaseRepository } from "../base";
import repository from "../repository";

export class ConversationRepository extends BaseRepository {
    resource = 'conversations';

    async fromFriend(id: number) {
        return await (await repository.get(`${this.resource}/from-friend/${id}`)).data;
    }
}