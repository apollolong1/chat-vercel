export const UserSocket = () => {
    const friendOnline = (e: any) => console.log(e)
    const addFriend = (e: any) => console.log(e)
    const answerFriendRequest = (e: any) => console.log(e)

    return { friendOnline, addFriend, answerFriendRequest }
}