import { useDispatch } from "react-redux"
import { useParams } from "react-router"
import { ReceiveCall } from "../redux/actions"

export const ConversationSocket = () => {
    const dispatch = useDispatch()

    const onAnswerCall = (e: any) => console.log(e)

    const onCallAccepted = (e: any) => {
        console.log('Đã nhận được tín hiệu từ người được gọi')
        console.log(e)
    }
    
    const onReceive = (e: any) => {
        console.log(e)
        console.log('Đã nhận được tín hiệu từ người gọi')
        dispatch(ReceiveCall(e.user.id, e.signal))
    } 

    return { onCallAccepted, onAnswerCall, onReceive }
}