type signData = {
    type: RTCSdpType,
    sdp: string | undefined
    candidates: RTCIceCandidate[]
} 

interface PeerEvents {
    signal: (data: signData) => void;
    stream: (stream: MediaStream) => void;
    connect: () => void; // update sau ( chạy khi 2 peer connect. Tìm hướng xử lý nhiều peer )
    data: (data: string | Buffer) => void; // update sau ( Nhận dữ liệu từ các peer khác. Dùng để cập nhật thời gian call hoặc lưu track )
    track: (track: MediaStreamTrack, stream: MediaStream) => void; // update ( chạy khi nhận được audio/video track. Tìm hướng xử lý multiphe tracks )
    close: () => void;
}

const config: RTCConfiguration = {
    iceServers: [
      {
        urls: "stun:116.118.49.35:3478",
      },
      {
        urls: "turn:116.118.49.35:3478",
        username: "techzonedigital",
        credential: "testingTZD",
      },
    ],
    // balanced là mặc định tất các ICE
    // max-bundle gộp các ICE tăng hiệu năng kết nối ICE nhưng giảm số lượng ICE
    // max-compat tăng khả năng tương thích thiết bị nhưng giảm hiệu năng kết nối ICE
    bundlePolicy: 'max-bundle',
    // all mặc định
    // relay chỉ sử dụng ICE từ TURN server
    iceTransportPolicy: 'relay',
    // kích thước lưu trữ ICE
    iceCandidatePoolSize: 0
};
  
export class Peer {
    private peerConnection: RTCPeerConnection;
    private initiator: boolean;
    private stream: MediaStream;
    // private candidatesHost: RTCIceCandidate[] = [];
    // private candidatesPrflx: RTCIceCandidate[] = [];
    // private canidatesRelay: RTCIceCandidate[] = [];
    // private canidatesSrflx: RTCIceCandidate[] = [];
    private sdp: RTCSessionDescriptionInit | null = null;
    private events: Partial<PeerEvents> = {};

    constructor(options: { initiator: boolean, stream: MediaStream }) {
        this.initiator = options.initiator;
        this.stream = options.stream
        this.peerConnection = new RTCPeerConnection(config);

        this.stream.getTracks().forEach((track) => {
            this.peerConnection.addTrack(track, this.stream);
        });
    
        this.peerConnection.onicegatheringstatechange = () => {
            switch(this.peerConnection.iceGatheringState) {
                case "new":
                    // The ICE agent has not started gathering candidates yet
                    break;
                case "gathering":
                    // The ICE agent is gathering candidates
                    break;
                case "complete":
                    // The ICE agent has finished gathering candidates
                    break;
                default: 
                    break;
              }
        }
        this.peerConnection.oniceconnectionstatechange = () => {
            switch(this.peerConnection.iceConnectionState) {
                case "new":
                    // The ICE agent is gathering addresses or waiting for remote candidates
                    break;
                case "checking":
                    // The ICE agent is checking the local and remote candidates for compatibility
                    break;
                case "connected":
                    // The ICE agent has found a usable connection for each component
                    break;
                case "completed":
                    // The ICE agent has finished gathering and checking candidates
                    break;
                case "failed":
                    // The ICE agent has failed to find a usable connection for each component
                    break;
                case "disconnected":
                    // The ICE agent has temporarily lost connectivity to the remote peer
                    break;
                case "closed":
                    // The ICE agent has shut down and is no longer responding to STUN requests
                    break;
                default:
                    break;
            }
        }

        this.peerConnection.onnegotiationneeded = async () => {
            if (this.initiator) {
                const offer = await this.peerConnection.createOffer();
                await this.peerConnection.setLocalDescription(offer);
                this.sdp = offer
            }
        }

        this.peerConnection.ontrack = (event) => {
            if (this.events.stream) {
                this.events.stream(event.streams[0]);
            }
        }

        // bundle ICE candidates
        this.peerConnection.onicecandidate = (event) => {
            const candidate = event.candidate
            if (candidate) {
                if(this.sdp) {
                    const data = {
                        type: this.sdp.type,
                        sdp: this.sdp.sdp,
                        candidates: [candidate]
                    };
                    if (this.events.signal) {
                        this.events.signal(data);
                    }
                }
                // this.candidatesHost.push(candidate)
                // if(candidate.type === 'host') {
                //     this.candidatesHost.push(candidate);
                // }
                // if(candidate.type === 'srflx') {
                //     this.canidatesSrflx.push(candidate);
                // }
                // if(candidate.type === 'prflx') {
                //     this.candidatesPrflx.push(candidate);
                // }
                // if(candidate.type === 'relay') {
                //     this.canidatesRelay.push(candidate);
                // }
                // if(
                //     this.candidatesHost.length > 0
                //     this.canidatesSrflx.length > 0 &&
                //     this.candidatesPrflx.length > 0 &&
                //     this.canidatesRelay.length > 0
                // ) {
                //     if(this.sdp) {
                //         const data = {
                //             type: this.sdp.type,
                //             sdp: this.sdp.sdp,
                //             candidates: [this.canidatesRelay[0]]
                //         };
                //         if (this.events.signal) {
                //             this.events.signal(data);
                //         }
                //     }
                // }
            }
        }
    }

    async signal(data: signData) {
        try {
            if(data.type === "offer") {
                await this.peerConnection.setRemoteDescription(new RTCSessionDescription({
                    type: data.type,
                    sdp: data.sdp
                }));
                const answer = await this.peerConnection.createAnswer();
                await this.peerConnection.setLocalDescription(answer);
                this.sdp = answer;
            }
            if(data.type === 'answer') {
                await this.peerConnection.setRemoteDescription(new RTCSessionDescription({
                    type: data.type,
                    sdp: data.sdp
                }));
            }
            if(data.candidates) {
                for(const candidate of data.candidates) {
                    await this.peerConnection.addIceCandidate(candidate);
                }
            }
        } catch (error) {
            console.error(error);
        }
    }

    on<K extends keyof PeerEvents>(event: K, handler: PeerEvents[K]) {
        this.events[event] = handler;
    }

    addStream(stream: MediaStream) {} //update sau ( thêm 1 stream )
    removeStream(stream: MediaStream) {} // update sau ( xóa 1 stream )
    addTrack(track: MediaStreamTrack, stream: MediaStream) {} // update sau ( thêm 1 track )
    removeTrack(track: MediaStreamTrack, stream: MediaStream) {} // update sau ( xóa 1 track )
    replaceTrack(oldTrack: MediaStreamTrack, newTrack: MediaStreamTrack, stream: MediaStream) {} // update sau ( replace track hiện tại với track mới )
    
    destroy() {
        this.peerConnection.close()
        this.events = {}
        this.sdp = null
        if(this.events.close) {
            this.events.close()
        }
    }
}
  