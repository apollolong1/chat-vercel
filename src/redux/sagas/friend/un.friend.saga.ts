import { SagaIterator } from "redux-saga";
import { call, put, takeLatest } from "redux-saga/effects";
import * as types from '../../types'
import { FactoryRepository } from "../../../services";

const repository = FactoryRepository.get('friendRequest')

const handle = async (friendId: number) => {
    return await repository.unFriend(friendId)
}

function* unFriend(action: any): SagaIterator {
    try {
        yield call(handle, action.payload)
        yield put({type: types.UN_FRIEND_SUCCESS, payload: action.payload})
    } catch (error) {
        yield put({type: types.UN_FRIEND_FAILED})
    } 
}

export function* watchUnFriend() {
    yield takeLatest(types.UN_FRIEND_REQUEST, unFriend)
}
