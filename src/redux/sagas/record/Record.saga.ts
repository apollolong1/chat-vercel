import { END, SagaIterator, buffers, eventChannel } from "redux-saga";
import { call, fork, put, race, take, takeLatest } from "redux-saga/effects";
import * as types from '../../types'

let mediaRecorder: MediaRecorder;
let mediaStream: MediaStream;

// Tạo một eventChannel để lấy dữ liệu âm thanh từ AnalyserNode
function createAudioDataChannel(analyser: AnalyserNode) {
    return eventChannel<number>(emitter => {
      const dataArray = new Uint8Array(analyser.frequencyBinCount);
  
      function emitAudioData() {
        analyser.getByteFrequencyData(dataArray);
        const average = dataArray.reduce((a, b) => a + b) / dataArray.length;
        emitter(average);
  
        requestAnimationFrame(emitAudioData);
      }
  
      emitAudioData();
  
      return () => {};
    }, buffers.sliding(1));
  }
  
  // Saga để xử lý việc cập nhật cường độ âm thanh
  function* handleAudioData(analyser: AnalyserNode): SagaIterator {
    const channel = yield call(createAudioDataChannel, analyser);
  
    while (true) {
      const { average, stop } = yield race({
        average: take(channel),
        stop: take(types.STOP_RECORD_SUCCESS)
      });
  
      if (stop) {
        break;
      }
  
      yield put({ type: types.UPDATE_AUDIO_LEVEL, payload: average });
    }
  }

function createGetUserMediaChannel(options: MediaStreamConstraints) {
  return eventChannel(emitter => {
    navigator.mediaDevices.getUserMedia(options)
      .then(stream => {
        emitter({ stream });
        emitter(END);
      })
      .catch(error => emitter({ error }));

    return () => {};
  });
}

function* startRecord(): SagaIterator {
    const channel = yield call(createGetUserMediaChannel, { audio: true });
    try {
        const { stream, error } = yield take(channel);
        if (error) {
            console.log(error);
            yield put({ type: types.START_RECORD_FAILED, payload: false });
        } else {
            mediaStream = stream;
            mediaRecorder = new MediaRecorder(stream);
            mediaRecorder.start();
            yield put({ type: types.START_RECORD_SUCCESS, payload: true });

            const audioContext = new AudioContext();
            const source = audioContext.createMediaStreamSource(stream);
            const analyser = audioContext.createAnalyser();
            source.connect(analyser);

            yield fork(handleAudioData, analyser);
        }
    } catch (error) {
        console.log(error)
        yield put({ type: types.START_RECORD_FAILED, payload: false })
    }
}

function* stopRecord(): SagaIterator {
    try {
        if (mediaRecorder && mediaRecorder.state === 'recording') {
            yield call([mediaRecorder, mediaRecorder.stop]);
            mediaStream.getTracks().forEach(track => track.stop());
            yield put({ type: types.STOP_RECORD_SUCCESS, payload: false });
        }
    } catch (error) {
        console.log(error);
        yield put({ type: types.STOP_RECORD_FAILED, payload: false });
    }
}

export function* watchStartRecord() {
    yield takeLatest(types.START_RECORD_REQUEST, startRecord)
}

export function* watchStopRecord() {
    yield takeLatest(types.STOP_RECORD_REQUEST, stopRecord)
}
