import * as types from '../types'

const initialState = {
    data: new Map(),
    listIds: [] as number[],
    next: true,
    loading: true,
    loadingMore: false,
    nextPage: 2
  };

export const FriendRequestReducer = (state = initialState, action:any) => {
    const { type, payload } = action
    let newData = new Map(state.data)
    let newListIds = [...state.listIds]
    let newNextPage = state.nextPage
    switch(type){
        case types.GET_FRIEND_REQUEST_SUCCESS:
            const { current, data, next } = payload
            data?.forEach((item:any) => {
                if(!newData.has(item.id)) {
                    newData.set(item.id, item)
                }
                return null
            });
            
            if(Number(current) === 1) {
                newListIds = data?.map(({id}: {id: number}) => id) || []
            }
            if(Number(current) > 1) {
                newListIds = [...newListIds, ...(data?.map(({id}: {id: number}) => id) || [])]
                newNextPage++
            }
            return {
                ...state,
                listIds: newListIds,
                data: newData,
                next: next,
                nextPage: newNextPage
            }
        case types.LOADING_FRIEND_REQUEST_ON:
        case types.LOADING_FRIEND_REQUEST_OFF:
            if(payload.page === 1) {
                return {
                    ...state,
                    loading: payload.check
                }
            }
            return {
                ...state,
                loadingMore: payload.check
            }
        case types.ACCESS_FRIEND_REQUEST_SUCCESS:
        case types.REJECT_FRIEND_REQUEST_SUCCESS:
            let indexlistIds = newListIds.indexOf(payload.id);
            if(newData.has(payload.id)) {
                if (indexlistIds > -1) {
                    newListIds.splice(newListIds.indexOf(payload.id), 1);
                    newData.delete(payload.id)
                }
            }
            return {
                ...state,
                listIds: newListIds,
                data: newData
            }
        case types.LOGOUT_SUCCESS:
            return initialState
        default:
            return state
    }
}
