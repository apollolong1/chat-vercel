import * as types from '../types'

export const login = (email: string, password: string) => ({
    type: types.LOGIN_REQUEST,
    payload: { email, password }
})

export const register = (fullName: string, phone: string, email: string, password: string) => ({
    type: types.REGISTER_REQUEST,
    payload: { fullName, phone, email, password }
})

export const logout = () => ({
    type: types.LOGOUT_REQUEST,
})

export const getProfile = () => ({
    type: types.GET_PROFILE_REQUEST
})

export const EditProfileRedux = (fullName: string, avatar: any) => ({
    type: types.EDIT_PROFILE_REQUEST,
    payload: { fullName, avatar }
})

export const setFormType = (type: "login" | "register" | "conversation" | "chat") => ({
    type: types.SET_FORM_TYPE,
    payload: type
})

