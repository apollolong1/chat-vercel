import * as types from '../types'

export const stopUploadFile = (uuid: string) => ({
    type: types.STOP_UPLOAD_CHUNK,
    payload: uuid
})